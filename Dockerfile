FROM openjdk:9.0.1-11-jre-slim
MAINTAINER aardelean <ardelean.i.alexandru@gmail.com>

ENV APP_NAME=aws-ci

COPY run.sh /
COPY build/libs/${APP_NAME}-*.jar /opt/

RUN find /opt -regextype egrep -regex ".*/${APP_NAME}-[0-9]+(\.[0-9]+)*(-PR[0-9]+)?(-SNAPSHOT)?\.jar" -exec echo {} \; -exec ln -s {} /opt/${APP_NAME}.jar \;

EXPOSE 8080

ENTRYPOINT [ "/run.sh" ]
