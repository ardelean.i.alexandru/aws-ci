package home.cvplayground;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("")
public class HealthRestEndpoint {
	@GetMapping()
	public String health() {
		return "status: up";
	}
}
